package be.kdg.java2.demo4jdbctemplatesave;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class PersonRepository {
    private static final Logger log = LoggerFactory.getLogger(PersonRepository.class);

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert personInserter;

    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.personInserter = new SimpleJdbcInsert(jdbcTemplate).withTableName("PERSONS").usingGeneratedKeyColumns("ID");
    }

    public List<Person> findByFirstName(String firstName){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE FIRSTNAME = ?",
                (rs, rowNum) -> new Person(rs.getInt("id"),
                        rs.getString("name"),
                        rs.getString("firstname")), firstName);
    }

    public Person findById(int id){
        return jdbcTemplate.queryForObject("SELECT * FROM PERSONS WHERE ID = ?",
                this::mapRow, id);
    }

    public List<Person> findByName(String name){
        return jdbcTemplate.query("SELECT * FROM PERSONS WHERE NAME = ?",
                new BeanPropertyRowMapper<>(Person.class), name);
    }

    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Person(rs.getInt("id"),
                rs.getString("name"),
                rs.getString("firstname"));
    }

    /*public Person save(Person person){
        jdbcTemplate.update("INSERT INTO PERSONS(NAME, FIRSTNAME) VALUES (?, ?)",
                person.getName(), person.getFirstName());
        //set the id of person??
        return person;
    }*/

    /*public Person save(Person person){
        PreparedStatementCreatorFactory pscf = new PreparedStatementCreatorFactory("INSERT INTO PERSONS(NAME, FIRSTNAME) VALUES (?, ?)"
                , Types.VARCHAR, Types.VARCHAR);
        pscf.setReturnGeneratedKeys(true);
        PreparedStatementCreator psc = pscf.newPreparedStatementCreator(Arrays.asList(person.getName(), person.getFirstName()));
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(psc, keyHolder);
        System.out.println(keyHolder.getKey());
        person.setId(keyHolder.getKey().intValue());
        return person;
    }*/

    public Person save(Person person){
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", person.getName());
        parameters.put("FIRSTNAME", person.getFirstName());
        person.setId(personInserter.executeAndReturnKey(parameters).intValue());
        return person;
    }
}
