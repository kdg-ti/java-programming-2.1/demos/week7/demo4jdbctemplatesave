package be.kdg.java2.demo4jdbctemplatesave;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo4jdbctemplatesaveApplication implements CommandLineRunner {
    @Autowired
    private PersonRepository personRepository;

    public static void main(String[] args) {
        SpringApplication.run(Demo4jdbctemplatesaveApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        personRepository.findByFirstName("JACK").forEach(System.out::println);
        System.out.println(personRepository.findById(1));
        personRepository.findByName("POTTER").forEach(System.out::println);
        Person saved = personRepository.save(new Person("DARKO", "DONNY"));
        System.out.println(saved);
        personRepository.findByFirstName("DONNY").forEach(System.out::println);
    }
}
